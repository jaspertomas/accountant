class HomeController < ApplicationController

  before_action :authenticate_user!, only: [:restricted]

  #home page, no login necessary
  def index
    #if signed in, send to proper dashboard page
    if user_signed_in? 
      redirect_to home_user_index_path 
    elsif admin_signed_in? 
      redirect_to home_admin_index_path 
    end
  end

  #user dashboard
  def user_index
    #if user not signed in, send to home page
    if !user_signed_in? 
      redirect_to home_index_path 
    end
  end

  #admin dashboard
  def admin_index
    #if not signed in as admin, maybe signed in as user
    #send to user dashboard
    if !admin_signed_in? 
      redirect_to home_user_index_path 
    end
  end

  def help
  end

  def contact
  end

  def restricted
  end
end

