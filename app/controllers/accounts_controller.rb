class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy, :recalc]

  # GET /accounts
  # GET /accounts.json
  def index
    @accounts = Account.all
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
    @account_entries=@account.account_entries.order('date desc,id desc').paginate(page: params[:page], per_page: Constants::RECORDS_PER_PAGE)
    @account_entry=AccountEntry.new
    @account_entry.account=@account
    @account_entry.date=Date.today
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)

    respond_to do |format|
      if @account.save
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search
    @searchstring=params['searchstring']
    
=begin
    #if searchstring is empty, do nothing
    if(@searchstring.empty?)
      @accounts=[]
      return
    end
=end
    
    @keywords=params['searchstring'].split(" ")

    #put together query as string
    query=""
    @keywords.each do |k|
      #add 'or' if query string is not empty
      if(!query.empty?) then query+=" or " end
      query+="name like '%#{k}%'"
      query+=" or description like '%#{k}%'"
    end  
    @accounts=Account.where(query).order(:name).paginate(page: params[:page], per_page: Constants::RECORDS_PER_PAGE)

    respond_to do |format|
        format.html { render :layout => !request.xhr? }
        # other formats
    end
  end

  def recalc
    @account.recalc
    redirect_to @account
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:name, :description, :balance)
    end
end
