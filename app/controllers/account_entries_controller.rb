class AccountEntriesController < ApplicationController
  before_action :set_account_entry, only: [:show, :edit, :update, :destroy]

  # GET /account_entries
  # GET /account_entries.json
  def index
    @account_entries = AccountEntry.all
  end

  # GET /account_entries/1
  # GET /account_entries/1.json
  def show
  end

  # GET /account_entries/new
  def new
    @account_entry = AccountEntry.new
  end

  # GET /account_entries/1/edit
  def edit
  end

  # POST /account_entries
  # POST /account_entries.json
  def create
    @account_entry = AccountEntry.new(account_entry_params)

    respond_to do |format|
      if @account_entry.save
        @account_entry.account.recalc
        format.html { redirect_to @account_entry.account, notice: 'Account entry was successfully created.' }
        format.json { render :show, status: :created, location: @account_entry }
      else
        format.html { render :new }
        format.json { render json: @account_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /account_entries/1
  # PATCH/PUT /account_entries/1.json
  def update
    respond_to do |format|
      if @account_entry.update(account_entry_params)
        @account_entry.account.recalc
        format.html { redirect_to @account_entry.account, notice: 'Account entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @account_entry }
      else
        format.html { render :edit }
        format.json { render json: @account_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /account_entries/1
  # DELETE /account_entries/1.json
  def destroy
    @account_entry.destroy
    @account_entry.account.recalc
    respond_to do |format|
      format.html { redirect_to @account_entry.account, notice: 'Account entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account_entry
      @account_entry = AccountEntry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_entry_params
      params[:account_entry][:amount]=params[:account_entry][:amount].sub(",","")
      params.require(:account_entry).permit(:account_id, :date, :amount, :balance, :description)
    end
end
