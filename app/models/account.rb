class Account < ApplicationRecord
  has_many :account_entries

  def recalc
    balance=0
    
    self.account_entries.order('date,id').each do |e|
      balance+=e.amount
      e.update(balance:balance)
    end
    
    self.update(balance:balance)
  end
end
