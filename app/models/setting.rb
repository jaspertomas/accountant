class Setting < ApplicationRecord
  validates :name, uniqueness: true, presence: true

  def self.get_value_for_name(name)
    setting=Setting.find_by_name(name)
    case(setting.kind)
    when 'String'
      setting.value
    when 'Integer'
      setting.value.to_i
    end
  end
end
