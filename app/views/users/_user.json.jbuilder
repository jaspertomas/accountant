json.extract! user, :id, :username, :firstname, :lastname, :phone, :email, :password, :password_confirmation, :active, :created_at, :updated_at
json.url user_url(user, format: :json)
