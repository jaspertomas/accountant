json.extract! account_entry, :id, :account_id, :date, :amount, :balance, :description, :created_at, :updated_at
json.url account_entry_url(account_entry, format: :json)
