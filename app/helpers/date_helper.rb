module DateHelper
  def self.to_pretty_date(date)
    date.strftime("%b %d, %Y") if(date)
  end
end
