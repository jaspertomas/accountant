Rails.application.routes.draw do
  get 'accounts/search'
  resources :account_entries
  resources :accounts do
    member do
      post 'recalc'
    end
  end
  devise_for :admins
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  #if logged in, root is home page
  authenticated :user do
    root to: 'home#user_index'
  end
  authenticated :admin do
    root to: 'home#admin_index'
  end
  #otherwise root is login page
  root to: redirect('/users/sign_in')

  devise_for :users
  get 'home/index'
  get 'home/user_index'
  get 'home/admin_index'
  get 'home/help'
  get 'home/contact'

  get '/restricted', to: 'home#restricted', as: :restricted

  get 'products/search'

  resources :settings, :except => [:new, :create, :destroy, :show]
  get 'products/quota_dashboard'
  resources :products  do
    member do
      get 'view_transactions_for' => 'products#transactions'
      get 'view_inventory_for' => 'products#inventory'
    end
  end
  resources :users
  resources :stocks  do
    member do
      get 'recalc' => 'stocks#recalc'
#      get 'view_stats_for' => 'stocks#stats'   #unused
    end
  end


end
