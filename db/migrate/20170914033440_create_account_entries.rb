class CreateAccountEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :account_entries do |t|
      t.references :account, foreign_key: true
      t.date :date
      t.decimal :amount, precision: 10, scale: 2
      t.decimal :balance, precision: 10, scale: 2
      t.string :description

      t.timestamps
    end
  end
end
