# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#Setting.create!(name: 'main_warehouse_id', value: '1', kind: 'Integer')

Admin.create!(username: 'admin', firstname: '', lastname: '', email: 'admin@admin.com', password:'admin', sign_in_count: 1, current_sign_in_at:'2016-10-03 08:42:23.951841', last_sign_in_at:'2016-10-03 08:37:24.108795', current_sign_in_ip:'127.0.0.1', last_sign_in_ip:'127.0.0.1', failed_attempts:0, created_at:'2016-10-03 08:37:23.918309',updated_at:'2016-10-03 08:42:23.958757')

User.create!(username: 'user', firstname: '', lastname: '', phone: '', email: 'user@user.com', password:'userr', sign_in_count: 1, current_sign_in_at:'2016-10-03 08:42:23.951841', last_sign_in_at:'2016-10-03 08:37:24.108795', current_sign_in_ip:'127.0.0.1', last_sign_in_ip:'127.0.0.1', failed_attempts:0, created_at:'2016-10-03 08:37:23.918309',updated_at:'2016-10-03 08:42:23.958757')

AccountEntry.create!( id: 1, account_id: 2, date: "2017-09-14", amount: 360633.49, balance: 360633.49, description: "Balance Forwarded", created_at: "2017-09-14 03:55:18", updated_at: "2017-09-14 04:26:00")           
AccountEntry.create!( id: 2, account_id: 2, date: "2017-09-02", amount: 3000, balance: 363633.49, description: "jen allowance", created_at: "2017-09-14 04:19:47", updated_at: "2017-09-14 04:26:17") 
AccountEntry.create!( id: 3, account_id: 2, date: "2017-09-03", amount: 1000, balance: 364633.49, description: "jen allowance", created_at: "2017-09-14 04:20:21", updated_at: "2017-09-14 04:24:23") 
AccountEntry.create!( id: 4, account_id: 2, date: "2017-09-12", amount: 1000, balance: 365633.49, description: "jen allowance", created_at: "2017-09-14 04:21:12", updated_at: "2017-09-14 04:26:29")
AccountEntry.create!( id: 5, account_id: 2, date: "2017-09-14", amount: 5000, balance: 370633.49, description: "dog vaccines", created_at: "2017-09-14 04:21:12", updated_at: "2017-09-14 04:26:29")
AccountEntry.create!( id: 6, account_id: 2, date: "2017-09-14", amount: -30000, balance: 0, description: "less mama computer", created_at: "2017-09-14 04:21:12", updated_at: "2017-09-14 04:26:29")

