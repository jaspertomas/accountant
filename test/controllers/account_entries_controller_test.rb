require 'test_helper'

class AccountEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @account_entry = account_entries(:one)
  end

  test "should get index" do
    get account_entries_url
    assert_response :success
  end

  test "should get new" do
    get new_account_entry_url
    assert_response :success
  end

  test "should create account_entry" do
    assert_difference('AccountEntry.count') do
      post account_entries_url, params: { account_entry: { account_id: @account_entry.account_id, amount: @account_entry.amount, balance: @account_entry.balance, date: @account_entry.date, description: @account_entry.description } }
    end

    assert_redirected_to account_entry_url(AccountEntry.last)
  end

  test "should show account_entry" do
    get account_entry_url(@account_entry)
    assert_response :success
  end

  test "should get edit" do
    get edit_account_entry_url(@account_entry)
    assert_response :success
  end

  test "should update account_entry" do
    patch account_entry_url(@account_entry), params: { account_entry: { account_id: @account_entry.account_id, amount: @account_entry.amount, balance: @account_entry.balance, date: @account_entry.date, description: @account_entry.description } }
    assert_redirected_to account_entry_url(@account_entry)
  end

  test "should destroy account_entry" do
    assert_difference('AccountEntry.count', -1) do
      delete account_entry_url(@account_entry)
    end

    assert_redirected_to account_entries_url
  end
end
